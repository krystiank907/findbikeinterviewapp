//
//  AppDelegate.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var applicationCoordinator: FBApplicationCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        #if DEBUG
        NetworkActivityLogger.shared.startLogging()
        #endif
        setupAppearance()
        createRootViewController()
        return true
    }

    private func createRootViewController() {
        if window == nil {
            window = UIWindow()
        }
        window?.makeKeyAndVisible()
        let rootVC = FBBaseNavigationController()
        window?.rootViewController = rootVC
        applicationCoordinator = FBApplicationCoordinator(navigationController: rootVC)
        applicationCoordinator?.start()
    }

    private func setupAppearance() {
        let navBarAppearance = UINavigationBar.appearance()
        navBarAppearance.barTintColor = .fbDarkGrey
        navBarAppearance.tintColor = .orange
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.orange]
    }
}

