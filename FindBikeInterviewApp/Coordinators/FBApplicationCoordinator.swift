//
//  FBApplicationCoordinator.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import UIKit


class FBApplicationCoordinator: FBBaseCoordinator<Void> {
    
    private let container = FBDependencyInjection.createChild()
    
    override func start() {
        showMain()
    }
    
    private func showMain() {
        guard let mainController = container.resolve(FBBikeListViewController.self) else { return }
        mainController.errorHandler = defaultErrorHandler
        mainController.navigationItemConfiguration = CommonNavigationBarConfiguration(title: "loc_station_list_title".localized)
        mainController.selectionHandler = { [weak self] data in
            self?.showDetailStation(data: data)
        }
        navigationController.viewControllers = [mainController]
    }
    
    private func showDetailStation(data: FBBikeDetailViewData) {
        guard let mainController = container.resolve(FBStationViewController.self, argument: data) else { return }
        mainController.errorHandler = defaultErrorHandler
        let leftNavigationButtonHandler: FBEmptyHandler = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        mainController.navigationItemConfiguration = CommonNavigationBarConfiguration(title: data.nameStation,
                                                                                      leftNavigationButtonImage: R.image.arrowLeft(),
                                                                                      leftNavigationButtonHandler: leftNavigationButtonHandler)
        navigationController.pushViewController(mainController, animated: true)
    }
    
}
