//
//  UITableViewCell+Ext.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import UIKit

extension UITableViewCell {
    static var standardIdentifier: String {
        return String(describing: self)
    }
}

