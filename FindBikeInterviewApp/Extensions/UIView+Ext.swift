//
//  UIView+Ext.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import UIKit

extension UIView {
    
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    func applyShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        offSet: CGSize = CGSize(width: 0, height: 2),
        blur: CGFloat = 4,
        spread: CGFloat = 0) {
        shadowColor = color
        shadowOpacity = alpha
        shadowOffset = offSet
        shadowRadius = blur / 2.0
        if spread == 0 {
            layer.shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            layer.shadowPath = UIBezierPath(rect: rect).cgPath
        }
        layer.masksToBounds = false
    }

}
