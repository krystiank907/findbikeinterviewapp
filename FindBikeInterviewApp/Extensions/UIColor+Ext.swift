//
//  UIColor+Ext.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import UIKit

extension UIColor {
    
    static var fbDarkGrey: UIColor {
        return UIColor(red: 0.25, green: 0.25, blue: 0.25, alpha: 1.00)
    }
    
    static var fbLimon: UIColor {
        return UIColor(red: 0.40, green: 0.82, blue: 0.59, alpha: 1.00)
    }
}
