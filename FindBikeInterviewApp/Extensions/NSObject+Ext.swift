//
//  NSObject+Ext.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import Foundation

extension NSObject {
    static var className: String {
        return "\(self)"
    }
}
