//
//  FBDependencyInjection.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import Foundation
import Swinject

class FBDependencyInjection {
    
    let container = Container()
    static let shared: FBDependencyInjection = FBDependencyInjection()
    
    class func createChild() -> Container {
        return Container(parent: shared.container)
    }
    
    init() {
        setupMainFlow()
    }
    
    private func setupMainFlow() {
        container.register(VMBikeListScheme.self) { _ in VMBikeList() }
        container.register(FBBikeListViewController.self) { res -> FBBikeListViewController in
            let viewController = FBBikeListViewController()
            viewController.viewModel = res.resolve(VMBikeListScheme.self)
            return viewController
        }
        container.register(VMStationDetailScheme.self) { (_, arg1: FBBikeDetailViewData) in
            return VMStationDetail(data: arg1)
        }
        container.register(FBStationViewController.self) { (res, arg1: FBBikeDetailViewData) -> FBStationViewController in
            let viewController = FBStationViewController()
            viewController.viewModel = res.resolve(VMStationDetailScheme.self, argument: arg1)
            return viewController
        }
    }
    
}


