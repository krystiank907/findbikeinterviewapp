//
//  FBBikesDTO.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import Foundation


struct FBBikesDTO: Codable {
    let features: [FBBikesFeatureDTO]
    let crs: FBBikesCRSDTO
    let type: String
}


struct FBBikesCRSDTO: Codable {
    let type: String
    let properties: FBBikesCRSProperties
}


struct FBBikesCRSProperties: Codable {
    let code: String
}


struct FBBikesFeatureDTO: Codable {
    let geometry: FBBikesFeatureGeometry
    let id: String
    let type: String
    let properties: FBBikesFeatureProperties
}

// MARK: - Geometry
struct FBBikesFeatureGeometry: Codable {
    let coordinates: [Double]
    let type: String
}

// MARK: - FeatureProperties
struct FBBikesFeatureProperties: Codable {
    let freeRacks, bikes, label, bikeRacks: String
    let updated: Date?

    enum CodingKeys: String, CodingKey {
        case freeRacks = "free_racks"
        case bikes, label
        case bikeRacks = "bike_racks"
        case updated
    }
}

