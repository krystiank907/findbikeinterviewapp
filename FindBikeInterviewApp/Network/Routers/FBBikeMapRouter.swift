//
//  FBBikeMapRouter.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import Foundation
import Alamofire

enum FBBikeMapRouter: Router {
    case getBikes
    
    var path: String {
        switch self {
        case .getBikes:
            return "/mim/plan/map_service.html?mtype=pub_transport&co=stacje_rowerowe"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .getBikes:
            return .get
        }
    }

    var parameters: Parameters {
        return [:]
    }

    var headers: HTTPHeaders? {
        return nil
    }

}
