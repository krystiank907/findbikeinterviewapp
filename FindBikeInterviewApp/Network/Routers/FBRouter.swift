//
//  FBRouter.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import Foundation
import Alamofire

protocol Router {
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: Parameters { get }
    var headers: HTTPHeaders? { get }
    var request: Request { get }
}

extension Router {
    var request: Request {
        return BaseRequest(endpointUrl: path,
                           parameters: parameters,
                           headers: headers)
    }
}
