//
//  FBErrors.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import Foundation

enum APIClientError: Error, Equatable {
    case networkError
    case commonError
}

