//
//  FBRequest.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import Foundation
import Alamofire

typealias RequestMultipartFormData = ((MultipartFormData) -> Void)?

protocol Request {
    var endpointUrl: String { get }
    var parameters: Parameters { get }
    var headers: HTTPHeaders? { get }
    var multipartFormData: RequestMultipartFormData { get }
}

struct BaseRequest: Request {
    var endpointUrl: String
    var parameters: Parameters
    var headers: HTTPHeaders?
    var multipartFormData: RequestMultipartFormData

    init(endpointUrl: String, parameters: Parameters, headers: HTTPHeaders?, multipartFormData: RequestMultipartFormData = nil) {
        self.endpointUrl = endpointUrl
        self.parameters = parameters
        self.headers = headers
        self.multipartFormData = multipartFormData
    }
}

