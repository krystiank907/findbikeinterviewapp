//
//  FBResponse.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import Foundation
import Alamofire

protocol Response {
    associatedtype CodableScheme: Codable

    var data: CodableScheme? { get set }
    var `internal`: HTTPURLResponse? { get set }
    var url: URL? { get set }
    var error: Error? { get }

    init()
    init(data: CodableScheme?, internal: HTTPURLResponse?, url: URL?)
}

extension Response {
    
    init(data: CodableScheme?, internal: HTTPURLResponse?, url: URL?) {
        self.init()
        self.data = data
        self.internal = `internal`
        self.url = url
    }

    var isSucceed: Bool {
        return (200..<300).contains(self.internal?.statusCode ?? 0)
    }
    
    var statusCode: Int {
        return self.internal?.statusCode ?? 0
    }
    
    var error: Error? {
        return baseError
    }
    
    var baseError: Error? {
        switch statusCode {
        case 400, 404:
            return APIClientError.commonError
        default:
            if data == nil {
                return APIClientError.commonError
            } else if !isSucceed {
                return APIClientError.commonError
            } else {
                return nil
            }
        }
    }
}
