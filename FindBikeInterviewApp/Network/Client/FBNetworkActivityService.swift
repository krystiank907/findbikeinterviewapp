//
//  FBNetworkActivityService.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import Foundation

protocol NetworkActivityServiceScheme {
    init(dispatchGroup: DispatchGroup?, onActivityStart: FBEmptyHandler, onActivityEnd: FBEmptyHandler, onAllActivitesEnd: FBEmptyHandler)

    func start()
    func stop()
    func notify()
}

class NetworkActivityService: NetworkActivityServiceScheme {
    private let onActivityStart: FBEmptyHandler
    private let onActivityEnd: FBEmptyHandler
    private let onAllActivitesEnd: FBEmptyHandler
    private let dispatchGroup: DispatchGroup?

    required init(dispatchGroup: DispatchGroup? = DispatchGroup(),
                  onActivityStart: FBEmptyHandler,
                  onActivityEnd: FBEmptyHandler,
                  onAllActivitesEnd: FBEmptyHandler = nil) {
        self.dispatchGroup = dispatchGroup
        self.onActivityStart = onActivityStart
        self.onActivityEnd = onActivityEnd
        self.onAllActivitesEnd = onAllActivitesEnd
    }

    func start() {
        dispatchGroup?.enter()
        onActivityStart?()
    }

    func stop() {
        dispatchGroup?.leave()
        onActivityEnd?()
    }

    func notify() {
        dispatchGroup?.notify(queue: .main, execute: { [weak self] in
            self?.onAllActivitesEnd?()
        })
    }
}
