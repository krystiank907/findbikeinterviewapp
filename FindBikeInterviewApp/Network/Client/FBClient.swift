//
//  FBClient.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import Foundation
import Alamofire

protocol FBClientConfiguration {
    var baseUrl: String { get set }
    var trustManager: ServerTrustManager? { get set }
}

struct APIClientConfiguration: FBClientConfiguration {
    var baseUrl: String
    var trustManager: ServerTrustManager?
}

protocol FBClientService {
    static var shared: FBClientService { get }

    func request<U: Response>(router: Router, activityService: NetworkActivityServiceScheme?, result: @escaping ResultCompletion<U>)
    func setConfiguration(_ configuration: FBClientConfiguration)
}

class APIClientService: FBClientService {

    private var configuration: FBClientConfiguration!

    private lazy var session: Alamofire.Session = {
        Alamofire.Session(configuration: APIClientService.configuration, serverTrustManager: configuration.trustManager)
    }()

    private static var configuration: URLSessionConfiguration {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 120
        configuration.timeoutIntervalForResource = 120
        configuration.requestCachePolicy = .reloadIgnoringCacheData
        return configuration
    }

    static var shared: FBClientService = APIClientService()


    func setConfiguration(_ configuration: FBClientConfiguration) {
        self.configuration = configuration
    }


    func createEndpoint(_ request: Request) -> String {
        return configuration.baseUrl + request.endpointUrl
    }

    func request<U>(router: Router, activityService: NetworkActivityServiceScheme?, result: @escaping (Result<U>) -> Void) where U: Response {

        let request = router.request
        let method = router.method
        let url = createEndpoint(request)
        let parameters = request.parameters.isEmpty ? nil : request.parameters
        
        DispatchQueue.main.async {
            activityService?.start()
        }

        session.request(url, method: method, parameters: parameters, encoding: Alamofire.JSONEncoding.default, headers: nil).response { (dataResponse) in
            let response = dataResponse.response
            let data = dataResponse.data

            if dataResponse.error != nil {
                DispatchQueue.main.async { result(.failure(APIClientError.networkError)) }
            } else if let data = data {
                var json: U.CodableScheme?
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(DateFormatter.fbAPIFormatter)
                    json = try decoder.decode(U.CodableScheme.self, from: data)
                } catch {
                    if let errorDes = error as? DecodingError {
                        print(errorDes.errorDescription ?? "")
                    }
                }
                let parsedResponse: U = U(data: json, internal: response, url: URL(string: url))
                DispatchQueue.main.async {
                    if let error = parsedResponse.error {
                        result(.failure(error))
                    } else {
                        result(.success(parsedResponse))
                    }
                }
            }

            DispatchQueue.main.async {
                activityService?.stop()
            }
        }
    }
    
}

