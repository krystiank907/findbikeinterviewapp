//
//  FBResult.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import Foundation

typealias ResultCompletion<T> = (Result<T>) -> Void

enum Result<T> {
    case success(T)
    case failure(Error)

    func resolve() throws -> T {
        switch self {
        case .success(let value):
            return value
        case .failure(let error):
            throw error
        }
    }

    var success: Bool {
        switch self {
        case .success:
            return true
        default:
            return false
        }
    }

    var error: Error? {
        switch self {
        case .success:
            return nil
        default:
            if case .failure(let error) = self {
                return error
            }
            return nil
        }
    }
}
