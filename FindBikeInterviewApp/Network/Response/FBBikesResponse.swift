//
//  FBBikesResponse.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import Foundation

struct FBBikesResponse: Response {
    typealias CodableScheme = FBBikesDTO

    var data: FBBikesDTO?
    var `internal`: HTTPURLResponse?
    var url: URL?

    init() {
        
    }
    
}
