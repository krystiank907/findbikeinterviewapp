//
//  FBWorld.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import Foundation
import Alamofire

protocol FBWorldScheme {
    static var shared: FBWorldScheme { get }

    var apiClient: FBClientService { get }
    var apiClientConfiguration: FBClientConfiguration { get }

}

struct FBWorld {
    let apiClient: FBClientService
    let apiClientConfiguration: FBClientConfiguration

    private init(apiClient: FBClientService, apiClientConfiguration: FBClientConfiguration) {
        self.apiClient = apiClient
        self.apiClientConfiguration = apiClientConfiguration
        self.apiClient.setConfiguration(apiClientConfiguration)
    }
}

extension FBWorld: FBWorldScheme {
    
    static var shared: FBWorldScheme = {
        let baseUrl = "http://www.poznan.pl"
        return FBWorld(apiClient: APIClientService.shared,
                       apiClientConfiguration: APIClientConfiguration(baseUrl: baseUrl))
    }()
    
}
