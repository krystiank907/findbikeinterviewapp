//
//  FBBaseNavigationController.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import UIKit

class FBBaseNavigationController: UINavigationController {
    
    var prefersNavigationBarHidden: Bool { return true }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(prefersNavigationBarHidden, animated: animated)
    }
}
