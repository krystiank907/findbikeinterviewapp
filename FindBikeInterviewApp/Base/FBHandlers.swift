//
//  FBHandlers.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import Foundation

typealias FBEmptyHandler = (() -> Void)?
typealias FBValueHandler<T> = ((T) -> Void)?
typealias FBOptionalValueHandler<T> = ((T?) -> Void)?
