//
//  FBBaseCoordinator.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import UIKit

class FBBaseCoordinator<ResultType> {
    
    private let identifier = UUID()
    
    weak var navigationController: UINavigationController!
    var childCoordinators = [UUID: Any]()
    var finishFlowHandler: FBValueHandler<ResultType?> = nil
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func run<T>(coordinator: FBBaseCoordinator<T>, triggerStart: Bool = true) {
        store(coordinator: coordinator)
        if triggerStart { coordinator.start() }
    }
    
    func start() {
        fatalError("It must be implemented in child class")
    }
    
    private func store<T>(coordinator: FBBaseCoordinator<T>) {
        childCoordinators[coordinator.identifier] = coordinator
    }
    
    func free<T>(coordinator: FBBaseCoordinator<T>) {
        childCoordinators[coordinator.identifier] = nil
    }
    
    func freeAll() {
        childCoordinators.removeAll()
    }
    
    var defaultErrorHandler: FBErrorHandleable {
        let errorHandler = FBErrorHandler()

        return errorHandler.catch(APIClientError.commonError, action: { [weak self] (_) in
            self?.showAlert(title: "loc_api_common_error".localized)
        }).catch(APIClientError.networkError, action: { [weak self] (_) in
            self?.showAlert(title: "loc_api_network_error".localized)
        })
    }
    
    func showAlert(title: String, message: String? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "loc_ok".localized, style: .default) { [weak alert] (_) in
            alert?.dismiss(animated: true, completion: nil)
        }
        alert.addAction(doneAction)
        navigationController.present(alert, animated: true, completion: nil)
    }
    
}
