//
//  FBNetworkViewModelScheme.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import Foundation

protocol FBNetworkViewModelScheme: class, FBBaseViewModel {
    var apiClient: FBClientService { get }
    var activityService: NetworkActivityServiceScheme? { get set }
    var activityIndicatorHandler: FBValueHandler<Bool> { get set }
    func setupActivityService()
}

extension FBNetworkViewModelScheme {
    var apiClient: FBClientService {
        return FBWorld.shared.apiClient
    }
}

extension FBNetworkViewModelScheme {
    func setupActivityService() {
        self.activityService = NetworkActivityService(onActivityStart: { [weak self] in
            self?.activityIndicatorHandler?(true)
        }, onActivityEnd: { [weak self] in
            self?.activityIndicatorHandler?(false)
        })
    }
}

