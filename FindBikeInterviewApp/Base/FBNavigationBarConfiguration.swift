//
//  FBNavigationBarConfiguration.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 18/01/2021.
//

import UIKit

protocol NavigationBarConfigurationScheme {
    func configureNavigationItem(_ navigationItem: UINavigationItem)
}

class CommonNavigationBarConfiguration: NavigationBarConfigurationScheme {
    private let title: String?

    private let leftNavigationButtonImage: UIImage?
    private let rightNavigationButtonImage: UIImage?

    private let leftNavigationButtonHandler: FBEmptyHandler
    private let rightNavigationButtonHandler: FBEmptyHandler
    

    init(title: String? = nil, leftNavigationButtonImage: UIImage? = nil, rightNavigationButtonImage: UIImage? = nil, leftNavigationButtonHandler: FBEmptyHandler = nil, rightNavigationButtonHandler: FBEmptyHandler = nil) {
        self.title = title
        self.leftNavigationButtonImage = leftNavigationButtonImage
        self.rightNavigationButtonImage = rightNavigationButtonImage
        self.leftNavigationButtonHandler = leftNavigationButtonHandler
        self.rightNavigationButtonHandler = rightNavigationButtonHandler
    }

    func configureNavigationItem(_ navigationItem: UINavigationItem) {
        let style: BaseNavigationBar.Style = .common(leftImage: leftNavigationButtonImage,
                                                     left: #selector(leftButtonAction),
                                                     title: title,
                                                     rightImage: rightNavigationButtonImage,
                                                     right: #selector(rightButtonAction))
        BaseNavigationBar.create(style: style, navigationBar: navigationItem, target: self)
    }

    @objc private func leftButtonAction() {
        leftNavigationButtonHandler?()
    }

    @objc private func rightButtonAction() {
        rightNavigationButtonHandler?()
    }
    
}

class BaseNavigationBar {
    
    enum Style {
        case common(leftImage: UIImage?, left: Selector, title: String?, rightImage: UIImage?, right: Selector)
    }
    
    static func create(style: Style, navigationBar: UINavigationItem, target: Any?) {
        switch style {
        case .common(let leftImage, let leftAction, let title, let rightImage, let rightAction):
            navigationBar.title = title

            if let leftImage = leftImage {
                let leftButton = UIBarButtonItem(image: leftImage, style: .done, target: target, action: leftAction)
                navigationBar.leftBarButtonItem = leftButton
            }
            if let rightImage = rightImage {
                let rightButton = UIBarButtonItem(image: rightImage, style: .done, target: target, action: rightAction)
                navigationBar.rightBarButtonItem = rightButton
            }
        }
    }
}
