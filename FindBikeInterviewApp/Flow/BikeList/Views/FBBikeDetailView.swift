//
//  FBBikeDetailView.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import UIKit
import MapKit
import RxSwift
import RxCocoa

struct FBBikeDetailViewData {
    var nameStation: String
    var coordinates: [Double]
    var availableBike: String
    var availablePlaces: String
    var userLocation: BehaviorRelay<CLLocation?>
}

class FBBikeDetailView: UIView {
    
    private let containerView: UIView = UIView()
    
    private let titleLabel: UILabel = UILabel()
    private let distanceLabel: UILabel = UILabel()
    private let streetLabel: UILabel = UILabel()
    private let dotLabel: UILabel = UILabel()
    private let itemsStackView: UIStackView = UIStackView()
    
    private let geoCoder = CLGeocoder()
    private let disposeBag: DisposeBag = DisposeBag()
    private var pointCoorindate: [Double]?
    private var userLocation: BehaviorRelay<CLLocation?>? {
        didSet {
            setupObservable()
        }
    }
    
    var canFetchAddress: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupObservable() {
        userLocation?.subscribe(onNext: { [weak self] _ in
            self?.setupDescriptionText()
        }).disposed(by: disposeBag)
    }
    
    
    private func setupViews() {
        containerView.clipsToBounds = false
        containerView.layer.cornerRadius = 10
        containerView.backgroundColor = .white
        containerView.applyShadow()
        itemsStackView.axis = .horizontal
        itemsStackView.alignment = .fill
        itemsStackView.distribution = .fillEqually
        titleLabel.font = .systemFont(ofSize: 26, weight: .bold)
        titleLabel.numberOfLines = 0
        distanceLabel.font = .systemFont(ofSize: 15, weight: .medium)
        dotLabel.textColor = .fbDarkGrey
        dotLabel.font = .systemFont(ofSize: 15, weight: .medium)
        streetLabel.textColor = .fbDarkGrey
        streetLabel.font = .systemFont(ofSize: 15, weight: .medium)
        streetLabel.numberOfLines = 0
    }
    
    private func makeConstraints() {
        addSubview(containerView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(distanceLabel)
        containerView.addSubview(dotLabel)
        containerView.addSubview(streetLabel)
        containerView.addSubview(itemsStackView)
        
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(15)
            make.leading.equalToSuperview().offset(19)
            make.trailing.equalToSuperview().offset(-19)
        }
        distanceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(streetLabel.snp.top)
            make.leading.equalToSuperview().offset(19)
            make.bottom.lessThanOrEqualTo(itemsStackView.snp.bottom).offset(-22)
        }
        dotLabel.snp.makeConstraints { (make) in
            make.top.equalTo(streetLabel.snp.top)
            make.leading.equalTo(distanceLabel.snp.trailing)
            make.bottom.lessThanOrEqualTo(itemsStackView.snp.bottom).offset(-22)
        }
        streetLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(2)
            make.leading.equalTo(dotLabel.snp.trailing)
            make.trailing.equalToSuperview().offset(-19)
        }
        itemsStackView.snp.makeConstraints { (make) in
            make.top.equalTo(streetLabel.snp.bottom).offset(22)
            make.leading.equalToSuperview().offset(19)
            make.trailing.equalToSuperview().offset(-19)
            make.bottom.equalToSuperview().offset(-15)
        }
    }
    
    private func userDistanceText(pointLocation: CLLocation) -> String {
        guard let userLocation = userLocation?.value else {
            return "loc_location_error".localized
        }
        let distance = userLocation.distance(from: pointLocation).rounded(toPlaces: 1)
        let measurementFormatter = MeasurementFormatter()
        measurementFormatter.numberFormatter.roundingMode = .up
        measurementFormatter.unitOptions = .providedUnit
        var metersValue = Measurement(value: distance, unit: UnitLength.meters)
        if distance > 1000 {
            metersValue.convert(to: .kilometers)
            measurementFormatter.numberFormatter.maximumFractionDigits = 1
        } else {
            measurementFormatter.numberFormatter.maximumFractionDigits = 0
        }
        let distanceText = measurementFormatter.string(from: metersValue)
        return distanceText
    }
    
    func setupData(data: FBBikeDetailViewData) {
        titleLabel.text = data.nameStation
        pointCoorindate = data.coordinates
        userLocation = data.userLocation
        itemsStackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        FBBikeItemStackViewType.allCases.forEach { (type) in
            let stackItemView = FBBikeItemStackView()
            stackItemView.setupData(type: type, count: type == .bike ? data.availableBike : data.availablePlaces)
            itemsStackView.addArrangedSubview(stackItemView)
        }
        if canFetchAddress {
            setupStreetLabel()
        }
    }
    
    private func setupDescriptionText() {
        guard let locationPoint = locationPoint else { return }
        distanceLabel.text = userDistanceText(pointLocation: locationPoint)
        layoutSubviews()
    }
    
    private func setupStreetLabel() {
        guard let locationPoint = locationPoint else { return }
        getCity(pointLocation: locationPoint) { [weak self] placeText in
            self?.dotLabel.text = "loc_bike_detail_dot".localized
            self?.streetLabel.text = placeText
        }
    }
    
    private func getCity(pointLocation: CLLocation, handler: FBValueHandler<String>) {
        geoCoder.reverseGeocodeLocation(pointLocation) { placemarks, _ in
            guard let placeMark = placemarks?.first else {
                return
            }
            var text: String = ""
            if let street = placeMark.thoroughfare {
                text += "\(street)"
            }
            if let city = placeMark.subAdministrativeArea {
                text += ", \(city)"
            }
            handler?(text)
        }
    }
    
    
    private var locationPoint: CLLocation? {
        guard let longitude = pointCoorindate?[safe: 0],
              let latitude = pointCoorindate?[safe: 1] else { return nil }
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
}
