//
//  FBBikeItemStackView.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import UIKit

enum FBBikeItemStackViewType: CaseIterable {
    case bike
    case places
    
    var iconImage: UIImage? {
        switch self {
        case .bike:
            return R.image.iconBike()
        case .places:
            return R.image.iconUnlock()
        }
    }
    var title: String {
        return "loc_item_type_\(self)".localized
    }
    var textColor: UIColor {
        switch self {
        case .bike:
            return .fbLimon
        case .places:
            return .fbDarkGrey
        }
    }
}

class FBBikeItemStackView: UIView {
    
    private let iconImageView: UIImageView = UIImageView()
    private let titleLabel: UILabel = UILabel()
    private let countLabel: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        iconImageView.contentMode = .scaleAspectFit
        titleLabel.textAlignment = .center
        titleLabel.textColor = .fbDarkGrey
        titleLabel.font = .systemFont(ofSize: 14, weight: .medium)
        countLabel.textAlignment = .center
        countLabel.font = .systemFont(ofSize: 50, weight: .bold)
    }
    
    private func makeConstraints() {
        addSubview(titleLabel)
        addSubview(iconImageView)
        addSubview(countLabel)
        
        iconImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(20)
            make.top.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(iconImageView.snp.bottom).offset(7)
            make.leading.trailing.equalToSuperview()
        }
        countLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(7)
            make.leading.bottom.trailing.equalToSuperview()
        }

    }
    
    func setupData(type: FBBikeItemStackViewType, count: String) {
        iconImageView.image = type.iconImage
        titleLabel.text = type.title
        countLabel.textColor = type.textColor
        countLabel.text = count
    }
}
