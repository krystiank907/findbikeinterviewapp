//
//  FBBikeListTableViewCell.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import UIKit

class FBBikeListTableViewCell: UITableViewCell {
    
    private let bikeContentView: FBBikeDetailView = FBBikeDetailView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        selectionStyle = .none
    }
    
    private func makeConstraints() {
        contentView.addSubview(bikeContentView)
        bikeContentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10))
        }
    }
    
    func setupData(data: FBBikeDetailViewData) {
        bikeContentView.setupData(data: data)
    }
    
    
}
