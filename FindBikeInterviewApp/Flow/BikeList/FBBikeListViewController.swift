//
//  FBBikeListViewController.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import UIKit
import MapKit
import SnapKit

class FBBikeListViewController: FBViewController<VMBikeListScheme> {
    
    private let tableView: UITableView = UITableView()
    private let locationManager = CLLocationManager()
    var selectionHandler: FBValueHandler<FBBikeDetailViewData> = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestForLocationServices()
        setupViews()
        makeConstraints()
        fetchData()
    }
    
    private func setupViews() {
        viewModel.activityIndicatorHandler = { [weak self] value in
            self?.handleByDefaultActivityIndicator(value)
        }
        viewModel.setupActivityService()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.register(FBBikeListTableViewCell.self, forCellReuseIdentifier: FBBikeListTableViewCell.standardIdentifier)
    }
    
    private func makeConstraints() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    private func fetchData() {
        viewModel.getData { [weak self] (result) in
            switch result {
            case .failure(let error):
                self?.errorHandler?.throw(error)
            case .success:
                self?.tableView.reloadData()
            }
        }
    }
    
    private func requestForLocationServices() {
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            locationManager.distanceFilter = 10
        }
    }
    
}
extension FBBikeListViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        viewModel.locationOfUser.accept(manager.location)
        tableView.reloadData()
    }
    
}

extension FBBikeListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FBBikeListTableViewCell.standardIdentifier) as? FBBikeListTableViewCell,
              let cellData = viewModel.cellData(for: indexPath) else {
            return UITableViewCell()
        }
        cell.setupData(data: cellData)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cellData = viewModel.cellData(for: indexPath) else { return }
        selectionHandler?(cellData)
    }
    
}
