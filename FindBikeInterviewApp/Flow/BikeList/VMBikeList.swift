//
//  VMBikeList.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import Foundation
import RxSwift
import RxCocoa
import MapKit

protocol VMBikeListScheme: FBNetworkViewModelScheme {
    var numberOfRows: Int { get }
    var locationOfUser: BehaviorRelay<CLLocation?> { get }
    func getData(completion: @escaping ResultCompletion<Void>)
    func cellData(for indexPath: IndexPath) -> FBBikeDetailViewData?
}

class VMBikeList: VMBikeListScheme {
    
    private var apiData: FBBikesDTO?
    var locationOfUser: BehaviorRelay<CLLocation?> = BehaviorRelay<CLLocation?>(value: nil)
    var activityService: NetworkActivityServiceScheme?
    var activityIndicatorHandler: FBValueHandler<Bool> = nil
    var numberOfRows: Int {
        return apiData?.features.count ?? 0
    }
    
    
    func getData(completion: @escaping ResultCompletion<Void>) {
        apiClient.request(router: FBBikeMapRouter.getBikes, activityService: activityService) { [weak self] (result: Result<FBBikesResponse>) in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let response):
                guard let data = response.data else {
                    completion(.failure(APIClientError.commonError))
                    return
                }
                self?.apiData = data
                completion(.success(()))
            }
        }
    }
    
    func cellData(for indexPath: IndexPath) -> FBBikeDetailViewData? {
        guard let featuresData = apiData?.features[safe: indexPath.row] else { return nil }
        return .init(nameStation: featuresData.properties.label,
                     coordinates: featuresData.geometry.coordinates,
                     availableBike: featuresData.properties.bikes,
                     availablePlaces: featuresData.properties.freeRacks,
                     userLocation: locationOfUser)
    }

}

