//
//  VMStationDetail.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import Foundation

protocol VMStationDetailScheme {
    var data: FBBikeDetailViewData { get }
    init(data: FBBikeDetailViewData)
}

class VMStationDetail: VMStationDetailScheme {
    var data: FBBikeDetailViewData
    
    required init(data: FBBikeDetailViewData) {
        self.data = data
    }
    
    
}
