//
//  FBMapPinView.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import UIKit

class FBMapPinView: UIView {
    
    private let containerView: UIView = UIView()
    private let iconImageView: UIImageView = UIImageView()
    private let countLabel: UILabel = UILabel()
    
    var title: String? {
        get {
            return countLabel.text
        } set {
            countLabel.text = newValue
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        containerView.clipsToBounds = false
        containerView.layer.cornerRadius = 12.5
        containerView.backgroundColor = .white
        iconImageView.image = R.image.iconBike()
        countLabel.textColor = .fbLimon
        countLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        containerView.applyShadow()
    }
    
    private func makeConstraints() {
        addSubview(containerView)
        addSubview(countLabel)
        containerView.addSubview(iconImageView)
        
        containerView.snp.makeConstraints { (make) in
            make.width.height.equalTo(25)
            make.leading.top.bottom.equalToSuperview()
        }
        iconImageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5))
        }
        countLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(containerView.snp.trailing).offset(5)
            make.top.bottom.trailing.equalToSuperview()
        }
    }
    
    
}
