//
//  FBStationAnnotationView.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import UIKit
import MapKit

class FBStationAnnotationView: MKAnnotationView {

    private let customPin = FBMapPinView()

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        frame = CGRect(x: 0, y: 0, width: 70, height: 25)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        backgroundColor = .clear
        addSubview(customPin)
        customPin.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    var title: String? {
        get {
            return customPin.title
        } set {
            customPin.title = newValue
        }
    }
}


class FBAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}
