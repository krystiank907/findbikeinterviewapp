//
//  FBStationViewController.swift
//  FindBikeInterviewApp
//
//  Created by Krystian Kulawiak on 19/01/2021.
//

import UIKit
import MapKit

class FBStationViewController: FBViewController<VMStationDetailScheme> {
    
    let mapView: MKMapView = MKMapView()
    let detailView: FBBikeDetailView = FBBikeDetailView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        makeConstraints()
    }
    
    private func setupViews() {
        detailView.canFetchAddress = true
        detailView.setupData(data: viewModel.data)
        mapView.showsUserLocation = true
        mapView.delegate = self
        mapView.register(FBStationAnnotationView.self, forAnnotationViewWithReuseIdentifier: FBStationAnnotationView.className)
        if let latitude = viewModel.data.coordinates[safe: 1], let longitude = viewModel.data.coordinates[safe: 0] {
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            mapView.setRegion(.init(center: coordinate,
                                    latitudinalMeters: .init(1500),
                                    longitudinalMeters: .init(1500)),
                              animated: true)
            mapView.addAnnotation(FBAnnotation(coordinate: coordinate))
        }
    }
    
    private func makeConstraints() {
        view.addSubview(mapView)
        view.addSubview(detailView)
        mapView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        detailView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(10)
            make.bottom.equalTo(view.snp.bottomMargin).offset(-10)
            make.trailing.equalToSuperview().offset(-10)
        }
    }
    
}

extension FBStationViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is FBAnnotation, let customAnnotation = mapView.dequeueReusableAnnotationView(withIdentifier: FBStationAnnotationView.className) as? FBStationAnnotationView {
            customAnnotation.title = viewModel.data.availableBike
            return customAnnotation
        } else {
            return nil
        }
    }
}
